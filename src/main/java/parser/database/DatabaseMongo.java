/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.database;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 *
 * @author tushar
 */
public class DatabaseMongo {

    private volatile static Datastore datastore = null;

    public static Datastore getDatastore() {
        if (datastore == null) {
            Morphia morphia = new Morphia();
            morphia.mapPackage("parser.entity");
            datastore = morphia.createDatastore(new MongoClient(), "translation");
            datastore.ensureIndexes();

        }
        return DatabaseMongo.datastore;

    }

}
