/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.utility;

import java.io.File;
import parser.common.Configuration;

/**
 *
 * @author tushar
 */
public class FileWriter {

    public static void writeSentense(String bangla, String english) {
        try {

            File archiveUrl = new File(Configuration.WRITTER_RESOURCE_LOC + "/sentense.txt");

            java.io.FileWriter fileWriter = new java.io.FileWriter(archiveUrl, true);
            fileWriter.write(bangla + " #~# " + english + "\n");
            fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
