/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.entity;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

/**
 *
 * @author tushar
 */

@Entity("sentence")
public class Sentence {
    @Id
    private ObjectId id;
    private String sentenceEng;
    private String sentenceBangla;

    @Override
    public String toString() {
        return "Sentence{" + "sentenceEng=" + sentenceEng + ", sentenceBangla=" + sentenceBangla + '}';
    }

    public Sentence() {
        this.sentenceEng = "";
        this.sentenceBangla = "";
        this.id = ObjectId.get();
    }

    public Sentence(String sentenceBangla, String sentenceEng) {
        this.sentenceBangla = sentenceBangla.trim();
        this.sentenceEng = sentenceEng.trim();
    }

    public String getSentenceBangla() {
        return sentenceBangla;
    }

    public void setSentenceBangla(String sentenceBangla) {
        this.sentenceBangla = sentenceBangla.trim();
    }

    public String getSentenceEng() {
        return sentenceEng;
    }

    public void setSentenceEng(String sentenceEng) {
        this.sentenceEng = sentenceEng.trim();
    }

}
