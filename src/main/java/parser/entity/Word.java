/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.entity;

import java.util.ArrayList;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Reference;

/**
 *
 * @author tushar
 */
@Entity("words")
@Indexes(@Index(value = "word", fields = @Field("word")))
public class Word {

    
    private String latter;
    @Id
    private String word;
    private String meaning;
    private String sourceUrl;
    private String wordUrl;
    private ArrayList<Sentence> sentenceList;

    @Override
    public String toString() {
        return "word{" + "id="  +"\nlatter=" + latter + "\n word=" + word + "\n meaning=" + meaning + "\n sourceUrl=" + sourceUrl + "\n wordUrl=" + wordUrl + "\n sentenceList=" + sentenceList + '}';
    }

    public Word() {
        this.latter = "";
        this.word = "";
        this.meaning = "";
        this.sourceUrl = "";
        this.wordUrl = "";
        this.sentenceList = new ArrayList<>();
//        this.id = ObjectId.get();
    }

    public ArrayList<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(ArrayList<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
    }

    public void setSentenceList(Sentence sentence) {
        this.sentenceList.add(sentence);
    }

    /**
     * Get the value of wordUrl
     *
     * @return the value of wordUrl
     */
    public String getWordUrl() {
        return wordUrl;
    }

    /**
     * Set the value of wordUrl
     *
     * @param wordUrl new value of wordUrl
     */
    public void setWordUrl(String wordUrl) {
        this.wordUrl = wordUrl;
    }

    /**
     * Get the value of sourceUrl
     *
     * @return the value of sourceUrl
     */
    public String getSourceUrl() {
        return sourceUrl;
    }

    /**
     * Set the value of sourceUrl
     *
     * @param sourceUrl new value of sourceUrl
     */
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    /**
     * Get the value of meaning
     *
     * @return the value of meaning
     */
    public String getMeaning() {
        return meaning;
    }

    /**
     * Set the value of meaning
     *
     * @param meaning new value of meaning
     */
    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    /**
     * Get the value of Word
     *
     * @return the value of Word
     */
    public String getWord() {
        return word;
    }

    /**
     * Set the value of Word
     *
     * @param word new value of Word
     */
    public void setWord(String word) {
        this.word = word;
    }

    public String getLatter() {
        return latter;
    }

    public void setLatter(String latter) {
        this.latter = latter;
    }

}
