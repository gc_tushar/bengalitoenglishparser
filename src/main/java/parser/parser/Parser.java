/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parser;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.log4j.LogManager;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mongodb.morphia.Datastore;
import parser.common.Fetcher;
import parser.database.DatabaseMongo;
import parser.entity.Sentence;
import parser.entity.Word;
import parser.utility.FileWriter;

/**
 *
 * @author tushar
 */
public class Parser {

    private static org.apache.log4j.Logger log = LogManager.getLogger(Parser.class);
    private static final Datastore datastore = DatabaseMongo.getDatastore();

    public static Word parserWordDetails(String url) {
        Word word = new Word();
        try {
            Document doc = Fetcher.fetch(url);
            Elements wordDetailElements = doc.select("#w_info");
            String wordDetails = wordDetailElements.text();

            word.setMeaning(wordDetails);
            word.setWordUrl(url);
            ArrayList<Sentence> sentenceList = parseTreanlation(doc);
            word.setSentenceList(sentenceList);
            datastore.save(sentenceList);
            doc = null;
            wordDetailElements = null;
            wordDetails = null;
            sentenceList = null;

        } catch (Exception e) {
            log.error(e);
        }
        return word;
    }

    public static ArrayList<Sentence> parseTreanlation(Document doc) {
        ArrayList<Sentence> sentenceList = new ArrayList<>();
        try {
            Elements translationElements = doc.select("#middle_area > div:nth-child(5) > div:nth-child(1) > div:nth-child(1)").select("ul>li");
            Sentence sentence = new Sentence();
            String banglaLine = "";
            String englishTranslation = "";
            for (Element e : translationElements) {
                try {
                    sentence = new Sentence();
                    banglaLine = e.select("span").text().trim();
                    e.select("span").remove();
                    englishTranslation = e.ownText().trim();
                    if (englishTranslation.startsWith("-")) {
                        englishTranslation = englishTranslation.substring(englishTranslation.indexOf("-") + 1);
                    }
                    sentenceList.add(new Sentence(banglaLine, englishTranslation));
                    FileWriter.writeSentense(banglaLine.trim(), englishTranslation.trim());
                } catch (Exception ex) {
                    log.error(ex);
                }
            }
            translationElements = null;
            banglaLine = null;
            englishTranslation = null;
        } catch (Exception e) {
            log.error(e);
        }
        doc = null;
        return sentenceList;
    }

    public static ArrayList<Word> parseWordList(String url, String latter) {

        ArrayList<Word> wordList = new ArrayList<>();
        Document doc = Fetcher.fetch(url);
        int lastpage = getLastPage(doc);
        Word word = new Word();
        String wordUrl = "";
        for (int i = 1; i <= lastpage; i++) {
            word = new Word();
            try {
                doc = Fetcher.fetch(url + "/" + i);
                Elements wordElements = doc.select("#cat_page > ul").select("li");
                for (Element e : wordElements) {
                    try {

                        wordUrl = e.select("a").attr("href");
                        word = parserWordDetails(wordUrl);
                        word.setWord(e.text());
                        word.setLatter(latter);
                        word.setSourceUrl(url + "/" + i);
                        //wordList.add(word);
                        datastore.save(word);
                    } catch (Exception ex) {
                        log.error(ex);
                    }

                }
            } catch (Exception e) {
                log.error(e);
            }
        }

        return wordList;
    }

    private static int getLastPage(Document doc) {
        int number = 100;
        try {
            Elements pagenationElements = doc.select(".pagination>a");
            String lastUrl = pagenationElements.last().attr("href");
            lastUrl = lastUrl.substring(lastUrl.lastIndexOf("/") + 1);
            if (lastUrl != null && !lastUrl.isEmpty()) {
                return Integer.parseInt(lastUrl);
            }
        } catch (Exception e) {
            log.error(e);
        }

        return number;
    }

    private static void parseDictionary() {
        Document doc = Fetcher.fetch("http://www.english-bangla.com/browse/bntoen");
        Elements latterElements = doc.select(".a-z").select("a");
        ArrayList<Word> allWords = new ArrayList<>();
        for (Element e : latterElements) {
            try {
                String latter = e.text();
                System.out.println("this latter: " + latter);
                String latterUrl = e.attr("href");
                latterUrl = dcodeUrl(latterUrl);
                ArrayList<Word> list = parseWordList(latterUrl, latter);
                allWords.addAll(list);
                for (int i = 0; i < list.size(); i++) {
                    System.out.println(list.get(i) + "\n");
                }
            } catch (Exception ex) {
                log.error(ex);
            }
        }
    }

    private static String dcodeUrl(String s) throws UnsupportedEncodingException {
        String latter = s.substring(s.lastIndexOf("/") + 1).trim();
        latter = URLEncoder.encode(latter, "utf-8");
        s = s.substring(0, s.lastIndexOf("/") + 1) + latter;
//        System.out.println("Final Encode: "+ s );
        return s;
    }

    public static void main(String[] args) {
        Parser parser = new Parser();
        parser.parseDictionary();
//        parser.parseWordList("http://www.english-bangla.com/browse/bntoen/অ", "আ");
//        parser.parserWordDetails("http://www.english-bangla.com/bntoen/index/%E0%A6%85%20%E0%A6%AC%E0%A6%BF%E0%A6%B6%E0%A7%8D%E0%A6%AC%E0%A6%BE%E0%A6%B8%E0%A7%80");
    }
}
