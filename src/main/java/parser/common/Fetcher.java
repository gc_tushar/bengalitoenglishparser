package parser.common;

import org.apache.log4j.LogManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author Tushar
 */
public class Fetcher {

    private static org.apache.log4j.Logger log = LogManager.getLogger(Fetcher.class);

    public static Document fetch(String url) {

        if (url == null || url.isEmpty()) {
            return null;
        }
        Document doc = null;
        int tryNumber = 0;

        while (tryNumber < Configuration.MAX_RETRY) {
            tryNumber++;
            log.debug("Try Number: " + tryNumber + " Fetching Content From Url: " + url);
            try {
                doc = Jsoup.connect(url).userAgent(Configuration.USER_AGENT).timeout(Configuration.CONNECTION_TIME_OUT).get();
                if (doc != null) {
                    break;
                } else {
                    try {
                        log.debug("Trying agin after " + Configuration.RETRY_DELAY_TIME + " mili sec");
                        Thread.sleep(Configuration.RETRY_DELAY_TIME);
                    } catch (InterruptedException ex) {
                        log.error(ex);
                    }
                }
            } catch (Exception e) {
                log.error(e);
            }
        }
        return doc;
    }

    public static void main(String[] args) {
        Fetcher fetcher = new Fetcher();
        System.out.println(fetcher.fetch("www.pipilika.com"));
    }

}
