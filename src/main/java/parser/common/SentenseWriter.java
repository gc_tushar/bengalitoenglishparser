package parser.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template startPositionFile, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tushar
 */
public class SentenseWriter {

    private static SentenseWriter singleToneStartPosWriter = null;
    public static RandomAccessFile startPositionFile = null;
    private static String writeResourceLocation = Configuration.WRITTER_RESOURCE_LOC + "/sentense.txt";
    private static FileWriter fileWriter;

    private SentenseWriter() {
    }

    public static synchronized SentenseWriter getInstance() throws IOException {

        if (singleToneStartPosWriter == null) {
            singleToneStartPosWriter = new SentenseWriter();
            startPositionFile = new RandomAccessFile(writeResourceLocation, "rws");

        }
        return singleToneStartPosWriter;
    }

    public static void writeStartPosFile(SentenseWriter writer, String curFile, String curBite) {
        try {
            writer.startPositionFile.seek(0);
            writer.startPositionFile.writeBytes(curFile + "\n" + curBite);
        } catch (Exception e) {
        }
    }

    synchronized public static void resetTheInstance() {
        try {
            startPositionFile.close();
            startPositionFile = null;
            singleToneStartPosWriter = null;
            
        } catch (Exception e) {
        }
    }

}
