package parser.common;

import java.util.HashMap;

/**
 *
 * @author Tushar
 */
public class Configuration {

    //Jsoup Parser
    public static final String USER_AGENT = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
    public static final int CONNECTION_TIME_OUT = 60 * 1000; // in milli secs6
    public static final int RETRIEVE_LIMIT = 2;
    public static final int RETRIEVE_TIMEOUT = 5 * 1000;  // in milli secs6
    public static final int RETRY_DELAY_TIME = 2000;
    public static final int MAX_RETRY = 1;
    //Resources Location
    public static final String WRITTER_RESOURCE_LOC = "resources";


}
